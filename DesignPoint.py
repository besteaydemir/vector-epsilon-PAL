import numpy as np

from Polyhedron import Polyhedron

class DesignPoint:

    def __init__(self, x: np.ndarray, R: Polyhedron):
        self.x = x
        self.R = R  # The confidence region (polyhedron)


    def __eq__(self, other):
        return (self.x == other.x).all()


    def __str__(self):
        if self.R.b is None:
            return "\nDesign Point: x " + str(self.x)
        name = "\nDesign Point: x " + str(self.x) + "\n" + str(self.R.b[0]) + " to " + str(-self.R.b[2]) +\
                "\n" + str(self.R.b[1]) + " to " + str(-self.R.b[3])
        return name


    def update_cumulative_conf_rect(self, mu, sigma, beta, C):
        # Generate Q_t polygon

        mu = mu.reshape(-1, )

        L = mu - sigma @ beta
        U = mu + sigma @ beta

        W = C.A

        QA = np.concatenate([W, -W])
        Qb = np.concatenate([W @ L, -W @ U])

        Q = Polyhedron(QA, Qb)


        self.R.intersect(Q)
        self.R.remove_redundant()
        self.R.update_vrep()

