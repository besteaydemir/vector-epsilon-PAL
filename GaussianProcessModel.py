import numpy as np
import gpflow
import tensorflow as tf
from gpflow.utilities import print_summary


class GaussianProcessModel:
    def __init__(self, m, d, kernel_list, noise_variance, x_sample = None, y_sample = None, verbose=False):
        """
        Class for handling GP objects.

        :param d: Input dimension.
        :param m: Output dimension.
        :param kernel_list: Kernel list given in the beginning.
        :param verbose:
        """

        self.m = m
        self.d = d

        self.noise_variance = noise_variance
        self.X = x_sample
        self.Y = y_sample


        # To initialize without data
        if x_sample is None:
            self.X = np.array([[-1e8] * self.d])
            self.Y = np.array([[0.] * self.m])


        self.kernel_list = kernel_list
        self.verbose = verbose
        self.opt_models = []

        self.model = self.single_output_gp_list(first=True)  # List of GP Models



    def single_output_gp_list(self, first=False):
        gp_list = []
        print("datanow")
        print(self.X)

        # Independent GP for each objective function
        for i in range(self.m):
            kernel = self.kernel_list[i]
            print(kernel.variance)
            m = gpflow.models.GPR(data=(self.X, self.Y[:, i].reshape(-1, 1)), kernel=kernel, noise_variance=self.noise_variance)

            if first:
                # Optimize the model for meaningful predictions (maximize the log marginal likelihood)
                gpflow.set_trainable(m.likelihood.variance, False)
                opt = gpflow.optimizers.Scipy()
                opt.minimize(m.training_loss, m.trainable_variables, options=dict(maxiter=100))

                self.opt_models.append(m)

            else:
                m = gpflow.models.GPR(data=(self.X, self.Y[:, i].reshape(-1, 1)), kernel=self.opt_models[i].kernel,
                                      noise_variance=self.noise_variance)

                # m = self.opt_models[i]


            gp_list.append(m)

            if self.verbose is True:
                print("For objective function ", i)
                print_summary(m)
                print("Log likelihood ", tf.keras.backend.get_value(m.log_marginal_likelihood()))


        return gp_list


    def multi_output_gp(self):
        return None


    def inference(self, x):
        mus = np.empty((self.m, 1))
        var = np.empty((self.m, 1))
        cov = np.zeros((self.m, self.m))
        x = x.reshape(1, -1)

        for i, gp in enumerate(self.model):
            a, b = gp.predict_f(x, full_cov=True)
            mus[i] = a
            cov[i, i] = b

        return mus, np.sqrt(var), cov


    def update(self, x, y):
        print(x.shape)
        print(self.X.shape)
        self.X = np.vstack((self.X, x))
        self.Y = np.vstack((self.Y, y))
        self.model = self.single_output_gp_list(first=False)
