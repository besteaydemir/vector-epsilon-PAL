import numpy as np


class OptimizationProblem:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.cardinality = len(self.x)


    def observe(self, point):
        print(point)
        index = np.where(self.x == point)
        return self.y[index]

