import gpflow as gpf
import tensorflow as tf

from VectorEpsilonPAL import VectorEpsilonPAL
from OptimizationProblem import OptimizationProblem
from GaussianProcessModel import GaussianProcessModel
from Polyhedron import Polyhedron
from utils import *
from utils_plot import *

from paretoset import paretoset
import pandas as pd


# Set seed
np.random.seed(4)
tf.random.set_seed(4)


# Generate two synthetic functions, f_1 and f_2
func1 = lambda x: 2 * np.sin(np.pi * x[:,0]) * np.sin(np.pi * x[:,1]) + 4 * np.sin(2 * np.pi * x[:,0]) * np.sin(2 * np.pi * x[:,1])
func2 = lambda x: 2 * np.sin(np.pi * x[:,0]) * np.sin(np.pi * x[:,1]) - 6 * np.sin(2 * np.pi * x[:,0]) * np.sin(2 * np.pi * x[:,1])


# Create the data set from the functions
d = 2  # The input dimension
m = 2  # The output dimension
n = 100  # The number of points

x = np.random.uniform(low = -1, high = 1, size = (n, d))
y = np.empty((n, m))
std = 0.01  # Noise std

y[:, 0] = func1(x) + np.random.randn() * std
y[:, 1] = func2(x) + np.random.randn() * std


# Create the OptimizationProblem object with a dataset (or modify the object's observe function to return observations)
problem_model = OptimizationProblem(x, y)


# Specify the kernel for GP prior
lengthscale_array = np.array([0.2, 0.2])  # Size d, tune a different lengthscale for each dimension (ARD)
kern_variance = 4
noise_variance = 0.1
kernel_list = [gpf.kernels.RBF(lengthscales=lengthscale_array, variance=kern_variance) for _ in range(2)]


# Choose 5 samples for initial GP (optional)
indexes = np.random.choice(len(x), 10, replace=False)
x_sample = x[indexes]
y_sample = y[indexes]

print("x_sample", x_sample)

# Create GaussianProcessModel object (GP handler)
gp = GaussianProcessModel(d = d, m = m, noise_variance = noise_variance, x_sample = x_sample, y_sample = y_sample,
                          kernel_list = kernel_list, verbose = True)


# Create the cone to be used in the algorithm
A = np.array([[-2, 0], [0, 1]])
b = np.array([0, 0])
C = Polyhedron(A = A, b = b)


# Adaptive Epsilon PAL algorithm
alg = VectorEpsilonPAL(problem_model = problem_model, cone = C, epsilon = 0.1, delta = 0.05, gp = gp)
pareto_set = alg.algorithm()


# Display Pareto set
print("Pareto set")
print_list(pareto_set)


pareto_points = [design_point.x for design_point in pareto_set]
a = np.squeeze(np.array(pareto_points)).reshape(-1, 2)
y1 = np.empty((len(pareto_points), m))

for i, row in enumerate(a):
    y1[i] = problem_model.observe(row)


func_val1 = y[:,0]
func_val2 = y[:,1]
hotels = pd.DataFrame({"func1": func_val1.reshape(-1,), "func2": func_val2.reshape(-1,)})
mask = paretoset(hotels, sense=["max", "max"])

title = "No. of evaluations: " + str(alg.t-2) + " | There exists Discard | " + r"$u$"
plot_pareto_front(func_val1, func_val2, mask, y1 = y1[:, 0], y2 = y1[:, 1],
                           plotfront=True, title=title)

