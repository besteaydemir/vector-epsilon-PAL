# vector-epsilon-PAL

Iimplementation of the vector-epsilon-PAL algorithm. 
main.py implements multi-objective vector optimization and identifies the Pareto set and the Pareto front for two synthetic objective functions.


# Dependencies

- cvxpy>=1.2.0
- gpflow>=2.5.1
- tensorflow>=2.8.0
- tensorflow-estimator>=2.4.0
- tensorflow-probability>=0.16.0
- paretoset>=1.2.0
- matplotlib>=3.3.4
- numpy>=1.21.6
