from phases import *
from Polyhedron import Polyhedron
from DesignPoint import DesignPoint
from utils import *

from utils_plot import *


class VectorEpsilonPAL:

    def __init__(self, problem_model, cone, epsilon, delta, gp):
        """
        VectorEpsilonPAL object.
        :param problem_model: OptimizationProblem object.
        :param cone: Polyhedron object.
        :param epsilon: epsilon parameter.
        :param delta: delta parameter.
        :param gp: GaussianProcessModel object.
        """

        self.problem_model = problem_model
        self.gp = gp
        self.cone = cone
        self.epsilon = epsilon
        self.delta = delta

        # Rounds
        self.t = 1  # Total number of iterations

        # Sets
        self.P = []  # Decided design points
        self.S = [DesignPoint(row, Polyhedron()) for row in problem_model.x]  # Undecided design points
        self.beta = np.ones(2, )



    def algorithm(self):
        """
        vector-epsilon-PAL algorithm.
        :return: List of DesignPoint objects.
        """

        # The region is a hyper-rectangle, set the cone as R+
        A_matrix = np.eye(self.gp.m)
        b_vector = np.array([0] * self.gp.m)
        cone = Polyhedron(A=A_matrix, b=b_vector)

        while len(self.S) > 1:  # While S_t is not empty
            print(self.t)
            # Active nodes, union of sets s_t and p_t at the beginning of round t
            A = self.P + self.S

            "Modeling"
            # Set beta for this round
            self.beta = self.find_beta(self.t)
            modeling(A, self.gp, self.beta, cone)  # TODO: Change this to hyperrectangle class


            "Discarding"
            discard(self.S, self.P, self.cone, self.epsilon)


            "epsilon-Covering"
            # The union of sets S and P at the beginning of epsilon-Covering
            W = self.S + self.P
            ecovering(self.S, self.P, self.cone, self.epsilon)


            "Evaluating"
            if self.S:  # If S_t is not empty
                x = evaluate(W)
                y = self.problem_model.observe(x.x)

                self.gp.update(x.x, y)

            self.t += 1

        return self.P


    def find_beta(self, t):
        beta = (1/18) * np.log(self.gp.m * self.problem_model.cardinality * np.pi ** 2 * t ** 2 / (6 * self.delta))

        return beta * np.ones(self.gp.m, )
