import cvxpy as cp
import numpy as np
import copy

from typing import List

from Polyhedron import Polyhedron
from DesignPoint import DesignPoint



def modeling(A, gp, beta, C):
    for point in A:
        mu, sigma, cov = gp.inference(point.x)
        point.update_cumulative_conf_rect(mu, cov, beta, C)




def discard(S, P, C, epsilon):
    A = S + P
    p_pess = pess(A, C)
    difference = set_diff(S, p_pess)


    for point in difference:
        for point_prime in p_pess:
            # Function to check if  ∃z' in R(x') such that R(x) <_C z + u, where u < epsilon
            # print("discard step")
            # print(point, point_prime)
            if dominated_by_opt2(point, point_prime, C, epsilon):

                S.remove(point)
                break



def ecovering(S, P, C, epsilon):
    A = S + P
    for point in S:
        exists = False
        for point_prime in A:
            if point == point_prime:
                continue
            # Function to check if  ∃x' in W_t  for epsilon-covering condition
            # print(point, point_prime)
            if ecovered(point, point_prime, C, epsilon):
                exists = True
                break

        # If the loop does not break, there is an x' in W_t
        if not exists:
            S.remove(point)
            P.append(point)

    return None


def evaluate(W: List[DesignPoint]) -> DesignPoint:
    largest = 0
    to_observe = None
    for x in W:
        diameter = x.R.diameter()

        if diameter > largest:
            largest = diameter
            to_observe = x

    return to_observe


def pess(point_set: List[DesignPoint], C: Polyhedron) -> List[DesignPoint]:
    """
    The set of Pessimistic Pareto set of a set of DesignPoint objects.

    :param point_set: List of Design objects.
    :param C: The ordering cone
    :return: List of Node objects.
    """
    pess_set = []
    length = len(point_set)

    for i in range(length):
        set_include = True

        for j in range(length):
            if j == i:
                continue

            # Check if there is another point j that dominates i, if so, do not include i in the pessimistic set
            if check_dominates(point_set[j].R, point_set[i].R, C):
                set_include = False
                break

        if set_include:
            pess_set.append(point_set[i])

    return pess_set


def check_dominates(polyhedron1: Polyhedron, polyhedron2: Polyhedron, cone: Polyhedron) -> bool:
    """
    Check if polyhedron1 dominates polyhedron2.
    Check if polyhedron1 is a subset of polyhedron2 + cone (by checking each vertex of polyhedron1).

    :param polyhedron1: The first polyhedron.
    :param polyhedron2: The second polyhedron.
    :param cone: The ordering cone.
    :return: Dominating condition.
    """

    condition = True
    n = cone.A.shape[1]  # Variable shape of x
    c = np.zeros(n)

    vertices = polyhedron1.get_vertices()

    for vertex in vertices:
        x = cp.Variable(n)
        y = cp.Variable(n)
        prob = cp.Problem(cp.Minimize(c.T @ x),
                          [x + y == vertex,
                           polyhedron2.A @ x >= polyhedron2.b,
                           cone.A @ y >= cone.b])
        prob.solve()

        if prob.status == 'infeasible':
            condition = False
            break


    return condition


def set_diff(s1, s2):  # Discarding
    """
    Set difference of two sets.

    :param s1: List of DesignPoint objects.
    :param s2: List of DesignPoint objects.
    :return: List of DesignPoint objects.
    """

    tmp = copy.deepcopy(s1)

    for node in s2:
        if node in tmp:
            tmp.remove(node)

    return tmp


def dominated_by(point, point_prime, C, epsilon):
    # Function to check if  ∃z' in R(x') such that R(x) <_C z + u, where u < epsilon (Alt1, check subset)
    # Define and solve the CVXPY problem.

    n = C.A.shape[1]

    u = cp.Variable(n)

    W_C = C.A
    b_C = C.b

    P = np.eye(n)

    # Check each vertex in R(x)
    condition = True
    vertices = point.R.get_vertices()
    for row in vertices:
        z = row

        #constraints = [W_C @ u >= b_C]  #TODO: With and without u \in C
        constraints = []
        for row_prime in point_prime.R.get_vertices():
            constraints.append(W_C @ z <= W_C @ (row_prime + u))

        prob = cp.Problem(cp.Minimize(cp.quad_form(u, P)),
                          constraints)
        prob.solve()

        if prob.value > epsilon**2:
            condition = False
            break

    return condition


def dominated_by_opt2(point, point_prime, C, epsilon):
    # Function to check if  ∃z' in R(x') such that R(x) <_C z + u, where u < epsilon (Alt3, check there exists)
    # Define and solve the CVXPY problem.

    n = C.A.shape[1]

    z_prime = cp.Variable(n)
    u = cp.Variable(n)

    W_point = point.R.A
    W_point_prime = point_prime.R.A
    W_C = C.A

    b_point = point.R.b
    b_point_prime = point_prime.R.b
    b_C = C.b

    P = np.eye(n)

    # Check each vertex in R(x)
    condition = True
    vertices = point.R.get_vertices()

    for row in vertices:
        z = row  # For all points in Rt, there exists a z_prime
        prob = cp.Problem(cp.Minimize(cp.quad_form(u, P)),
                          [W_C @ z <= W_C @ (z_prime + u),
                           W_point_prime @ z_prime >= b_point_prime])
                           #W_C @ u >= b_C])
        prob.solve(solver=cp.ECOS)

        # Print result.
        # print("\nThe optimal value is", prob.value)
        # print(u.value)

        if prob.value > epsilon**2: #isclose?
            condition = False
            break

    return condition


def ecovered(point, point_prime, C, epsilon):
    """

    :param point: DesignPoint x.
    :param point_prime: Design Point x'.
    :param C: Polyhedron C.
    :param epsilon:
    :return:
    """
    n = C.A.shape[1]

    z = cp.Variable(n)
    z_point = cp.Variable(n)
    z_point2 = cp.Variable(n)
    c_point = cp.Variable(n)
    c_point2 = cp.Variable(n)
    u = cp.Variable(n)

    W_point = point.R.A
    W_point_prime = point_prime.R.A
    W_C = C.A

    b_point = point.R.b
    b_point_prime = point_prime.R.b
    b_C = C.b

    P = np.eye(n)

    prob = cp.Problem(cp.Minimize(cp.quad_form(u, P)),
                      [z == z_point + u + c_point,
                       z == z_point2 - c_point2,
                       W_point @ z_point >= b_point,
                       W_point_prime @ z_point2 >= b_point_prime,
                       W_C @ c_point >= b_C,
                       W_C @ c_point2 >= b_C])
                       #W_C @ u >= b_C])
    prob.solve(solver=cp.ECOS)

    # Print result.
    # print("\nThe optimal value is", prob.value)
    # print(u.value)

    condition = prob.value < epsilon ** 2  # isclose?

    return condition


def find_farthest(point):

    # Brute force for now
    vertices = point.R.get_vertices()

    no_vertices = len(vertices)
    largest = 0
    pair = None
    for i in range(no_vertices):
        for j in range(i, no_vertices):
            dist = np.linalg.norm(vertices[i] - vertices[j])

            if dist > largest:
                largest = dist
                pair = (i, j)

    return largest


def cone_order(x, y, cone):
    """
    Check if x <_C y
    :param point1: x
    :param point2: y
    :param cone: C
    :return:
    """
    W = cone.W
    z = y - x

    return np.all(W @ z > 0)

